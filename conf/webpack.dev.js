const webpack = require("webpack");
const { merge } = require("webpack-merge");
const common = require("./webpack.common");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = merge(common, {
  entry: {
    app: [
      "webpack-hot-middleware/client?reload=true&timeout=1000",
      path.join(__dirname, "../server/index.ts"),
    ],
  },
  mode: "development",
  devtool: "source-map",
  devServer: {
    baseContent: path.join(__dirname, "../dist"),
    port: process.env.app_port,
    host: process.env.app_host,
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.join(__dirname, "../public/index.html"),
    }),
  ],
});
