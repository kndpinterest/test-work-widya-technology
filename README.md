#### how to use

- mv file .env.example to .env
- npm install
- npm run test
- run application to development -> npm run nodemon

- output webpack dev & prod

![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/164622588_1372434129756671_7822730828284063627_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeHQyUz2xrBVbV5ckhIjZZDAb99w7SsQ81xv33DtKxDzXB5x7umvPF6jYfwZE5GbSsqdBeyvNq_DxZqcYxO7DneB&_nc_ohc=Nkzv4h0DmlcAX-LZFip&_nc_ht=scontent.fsrg1-1.fna&oh=66828bdc893c6a7db5fae42ee82b2d49&oe=60800A00)

- testing

![alt text](https://scontent.fjog3-1.fna.fbcdn.net/v/t1.0-9/164614093_1373739942959423_9006228145190392151_o.jpg?_nc_cat=108&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeG9UcMr3ooPOV3BjsNh9VtKUAYKW8-R2ZZQBgpbz5HZlvaBgzE8wp_SYC4PEcOrPzNxhsb86MbXcwlLGcrbEfwk&_nc_ohc=KeTxJdwaqpsAX-wntCR&_nc_ht=scontent.fjog3-1.fna&oh=af74a983acb04615b127d6ac0c0f6e5e&oe=60845819)

- insomnia

![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/165405015_1373886129611471_1400770607951767582_o.jpg?_nc_cat=106&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeGoOro758GRjAOYdIXm0mgsxXDLplt845PFcMumW3zjkzEenOJHrMSk9nR2F8uVb56xGh1uIgabsv-d8KtP0_Lh&_nc_ohc=zA5HegdmgtcAX_RRLLJ&_nc_ht=scontent.fsrg1-1.fna&oh=6fca827b60a9fdef3fa2eba8e74e8746&oe=60826621)
![alt text](https://scontent.fsrg1-1.fna.fbcdn.net/v/t1.0-9/165874711_1373886126278138_3407108770992034509_o.jpg?_nc_cat=104&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeGQ5RzQs4as7G2SxSCHxIvyf0pXdUoF4UZ_Sld1SgXhRs1Iej0A2zpnboLt02oUcF-HLfTO1C1vPCKrwy2R_BrB&_nc_ohc=ZJuybj4wXtwAX_XgZ78&_nc_ht=scontent.fsrg1-1.fna&oh=dc42320f60d4773be5f106a8386496a6&oe=6082C610)

- how to log in with insomnia or postman, first you have to use the npm run test, because you need user data when it's finished you can open mysql

```
mysql -u root -p
-> your_password
-> select user.username from user;
```

- here you need a username for login and for a login user password -> Password

This app still has errors in testing jest

- kegunaan JSON didalam rest api

```
Javascript Object Notation / JSON -> adalah sebuah file system yang bisa digunakan untuk
membantu menjalankan system aplikasi yang berkaitan dengan javascript/typescript
dan JSON juga bisa dijadikan sebagai database dan JSON bisa juga dijadikan sebagai tools requirements
untuk membantu developer menemukan bug seperti lupa memberi tanda semicolon (;),
contoh file didalam javascript eslint.json & typescript tslint.json, dan JSON juga bisa
jadikan sebagai file system membantu meningkatkan kinerja aplikasi yaitu babel.config.json atau .babelrc
babel merupakan plugin javascript compiler yang bisa mengubah kode yang ditulis menggunakan es6/ecmascript6 menjadi
kode yang bisa dijalankan didalam browser yang tidak atau belum mendukung es6 dan masih banyak lagi kegunaannya.
```

- Buat valiasi password dengan Node.js

```
didalam dunia programming password adalah hal yang saya penting karena
ini berhubungan dengan data user yang dapat membantu masuk kedalam system,
biasa untuk parah programmer membuat 2 form yaitu password dan konfirmasi password
alasan membuat ini untuk membantu pengguna aplikasi agar tidak terjadi kesalahan
ketika membuat sebuah password dan didalam script programmer akan membuat sebuah
script conditional statement atau if-else, ketika password tersebut dinyata benar
maka secara otomatis programmer akan membuat password tersebut menjadi hash/key
atau dijadikan text tersebut menjadi kode rahasia
contoh simple your_password => b'$2b$12$oXnNOnzOUMGOwhNFqxMwlut0ifAuAjzArElcdM4ybCSHyyluze4vi',
ketika password tersebut don't match / tidak cocok maka programmer akan memberi script throw Error("Password don't match")
script init berfunsi untuk membuat sebuah valiasi error yang akan dikirim langsung kepada client.
```

- Berikan contoh query Mysql berupa 1 table database yang berisikan; id produk, nama produk dan harga produk!
  ![alt text](https://scontent.fcgk3-1.fna.fbcdn.net/v/t1.6435-9/166174699_1376203699379714_9004666194842445892_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeFNyRBGWm6TVgWg_ulBIvswcgIP1P4YVZNyAg_U_hhVkwEfaGErDedpqfKzPtq8pcAp-m20HTj6k8oyU0kwainy&_nc_ohc=GGNjGzynyPcAX_XPPba&_nc_ht=scontent.fcgk3-1.fna&oh=97367badc795ea6a047ce51c0914f0a8&oe=608A3A97)

- Buatlah REST API untuk menentukan besaran total produk yang memiliki harga diatas 80000 dalam satu table dengan menggunakan node.js!

![alt text](https://scontent.fcgk7-1.fna.fbcdn.net/v/t1.6435-9/165920772_1376210909378993_605391966381319604_n.jpg?_nc_cat=100&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeGxoA8ESijM3c-0zolHce1yvtS9vQaeUge-1L29Bp5SBy4HG6jNN6Qd_ncr0VJ0d_B2oE13ErpmQnmWNjcPf_wZ&_nc_ohc=-sWL4caU2sIAX9l_1O6&_nc_ht=scontent.fcgk7-1.fna&oh=35fa28a78ec98bd054d5a1673e952dba&oe=6086953A)

![alt text](https://scontent.fcgk7-2.fna.fbcdn.net/v/t1.6435-9/166330290_1376210902712327_8215300542256997966_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeFIInrmwkDvgbNse9nd3xA-U2azCDm6wqBTZrMIObrCoAuiiHKfSiwfQeY6y98TIqqf05LHWKzWIAeEe3eGH4go&_nc_ohc=zoeKwDYpUc8AX-AUNi0&_nc_ht=scontent.fcgk7-2.fna&oh=8ee20597786971e65dbdf551f4546613&oe=60884D87)

![alt text](https://scontent.fcgk3-2.fna.fbcdn.net/v/t1.6435-9/166797988_1376210879378996_7323629741119551856_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeGfP1CSzKbewh_PtmtvVsHd_fBARuo8ii_98EBG6jyKL7OYnvCAMU67b0dOQZXAvnWxtymSwlpRa46dtPd0qc1-&_nc_ohc=BxLozwGGcWkAX-KHKwu&_nc_ht=scontent.fcgk3-2.fna&oh=9a589845244ac42347f72e2e371ff22e&oe=60870092)
