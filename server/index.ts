import { Connection, createConnection, useContainer } from "typeorm";
import { Application } from "./utils/xconfig";
import Con from "./utils/tconfig";
import { Container } from "typeorm-typedi-extensions";

useContainer(Container);

export const con = (): Promise<Connection> => {
  return createConnection(Con)
    .then(async (con) => {
      if (!process.env.TESTING) {
        console.log("Connection DB");
      }
      return con;
    })
    .catch((err) => {
      if (!process.env.TESTING) {
        console.log("DB ERROR");
      }
      return err;
    });
};

con();

export const app = new Application();

app.listen();
