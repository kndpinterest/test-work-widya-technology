import supertest from "supertest";
import { Connection } from "typeorm";
import { app, con } from "..";
import { CategoryProductEntity } from "../typeorm/entity/CategoryProductEntity";
import { ProductEntity } from "../typeorm/entity/ProductEntity";
import { token } from "../utils-test/setup";
import faker from "faker";
import path from "path";

let coon: Connection;

beforeAll(async () => {
  coon = await con();
});

afterAll(async () => {
  coon.close();
});

test("Get ALL PRODUCT", async (done) => {
  supertest(app.app)
    .get("/api/product/")
    .set("Accept", "application/json")
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res) => {
      expect(res.status).toEqual(200);
      expect(res.body).not.toEqual(null);
      return done();
    });
});

test("Record Product", async (done) => {
  const category = await CategoryProductEntity.findOne({
    order: {
      id: "ASC",
    },
  });
  const product = await ProductEntity.count();
  if (category) {
    supertest(app.app)
      .post("/api/product/")
      .set("Authorization", `Bearer ${token}`)
      .attach(
        "photo",
        path.join(__dirname, "../utils-test/images/download.jpeg")
      )
      .field("category", category.name)
      .field("product_name", faker.name.title())
      .field("item", faker.name.jobTitle())
      .field("price_sell", faker.random.number(12000))
      .field("price_promo", faker.random.number(12000))
      .field("price_agent", faker.random.number(12000))
      .field("type", faker.name.jobType())
      .field("weight", faker.random.number(8000))
      .field("sku", product + 1)
      .field("recommendation", faker.random.number(1))
      .field("description", faker.lorem.text())
      .expect(200)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body).toEqual({
          data: {
            status: "Success",
            statusCode: 201,
            message: "Product has been created",
          },
        });
      });
  }
});

// test("Delete Product", async (done) => {
//   const check = await ProductEntity.findOne();
//   supertest(app.app)
//     .delete(`/api/product/${check.id}/`)
//     .set("Accept", "application/json")
//     .set("Authorization", `Bearer ${token}`)
//     .expect(200)
//     .then((res) => {
//       expect(res.status).toEqual(200);
//       expect(res.body).toEqual({
//         data: {
//           message: "Product has been deleted",
//           statusCode: 200,
//           status: "Success",
//         },
//       });
//       return done();
//     });
// });

// test("Update Product", async (done) => {
//   const check = await ProductEntity.findOne();
//   const category = await CategoryProductEntity.findOneOrFail();
//   supertest(app.app)
//     .post(`/api/product/${check.id}/`)
//     .set("Authorization", `Bearer ${token}`)
//     .attach("photo", path.join(__dirname, "../utils-test/images/download.jpeg"))
//     .field("category", category.name)
//     .field("product_name", faker.name.title())
//     .field("item", faker.name.jobTitle())
//     .field("price_sell", faker.random.number(12000))
//     .field("price_promo", faker.random.number(12000))
//     .field("price_agent", faker.random.number(12000))
//     .field("type", faker.name.jobType())
//     .field("weight", faker.random.number(8000))
//     .field("recommendation", faker.random.number(1))
//     .field("description", faker.lorem.text())
//     .expect(200)
//     .then((res) => {
//       expect(res.status).toEqual(200);
//       expect(res.body).toEqual({
//         data: {
//           message: "Product has been updated",
//           status: "Success",
//           statusCode: 200,
//         },
//       });
//       return done();
//     });
// });

// test("Detail Product", async (done) => {
//   const product = await ProductEntity.findOne();
//   if (product) {
//     supertest(app.app)
//       .get(`/api/product/${product.id}/`)
//       .set("Accept", "application/json")
//       .set("Authorization", `Bearer ${token}`)
//       .expect(200)
//       .then((res) => {
//         expect(res.status).toEqual(200);
//         expect(res.body).not.toEqual(null);
//         return done();
//       });
//   }
// });
