import supertest from "supertest";
import faker from "faker";
import { Connection } from "typeorm";
import { app, con } from "..";
import { token } from "../utils-test/setup";
import { CategoryProductEntity } from "../typeorm/entity/CategoryProductEntity";

let coon: Connection;

beforeAll(async () => {
  coon = await con();
});

afterAll(async () => {
  coon.close();
});

test("Record Category", async (done) => {
  supertest(app.app)
    .post("/api/category/")
    .send({
      name: faker.name.title(),
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .expect(200)
    .then((res) => {
      expect(res.status).toEqual(200);
      expect(res.body).toEqual({
        data: {
          message: "Category Product has been created",
          status: "Success",
          statusCode: 201,
        },
      });
      return done();
    });
});

test("Get All Category Product", async (done) => {
  supertest(app.app)
    .get("/api/category/")
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .expect(200)
    .then((res) => {
      expect(res.status).toEqual(200);
      expect(res.body).not.toEqual(null);
      return done();
    });
});

// test("Delete Category Product", async (done) => {
//   const category = await CategoryProductEntity.findOne({
//     order: {
//       id: "DESC",
//     },
//   });
//   if (category) {
//     supertest(app.app)
//       .delete("/api/category/" + category.id + "/")
//       .set("Authorization", `Bearer ${token}`)
//       .set("Accept", "application/json")
//       .expect(200)
//       .then((res) => {
//         expect(res.status).toEqual(200);
//         expect(res.body).toEqual({
//           data: {
//             message: "Category Product has been deleted",
//             status: "Success",
//             statusCode: 200,
//           },
//         });
//         return done();
//       });
//   }
// });

// test("Update Category Product", async (done) => {
//   const category = await CategoryProductEntity.findOne({
//     order: {
//       id: "ASC",
//     },
//   });
//   if (category) {
//     supertest(app.app)
//       .put(`/api/category/${category.id}/`)
//       .send({
//         name: faker.name.title(),
//       })
//       .set("Authorization", `Bearer ${token}`)
//       .set("Accept", "application/json")
//       .then((res) => {
//         expect(res.status).toEqual(200);
//         expect(res.body).toEqual({
//           data: {
//             message: "Category Product has been updated",
//             statusCode: 200,
//             status: "Success",
//           },
//         });
//         return done();
//       });
//   }
// });

// test("Detail Category Product", async (done) => {
//   const category = await CategoryProductEntity.findOne({
//     order: {
//       id: "ASC",
//     },
//   });
//   if (category) {
//     supertest(app.app)
//       .get(`/api/category/${category.id}/`)
//       .set("Authorization", `Bearer ${token}`)
//       .set("Accept", "application/json")
//       .expect(200)
//       .then((res) => {
//         expect(res.body).not.toEqual(null);
//         expect(res.status).toEqual(200);
//         return done();
//       });
//   }
// });
