import { app, con } from "..";
import faker from "faker";
import supertest from "supertest";
import { Connection } from "typeorm";
import { UserEntity } from "../typeorm/entity/UserEntity";

let coon: Connection;

beforeAll(async () => {
  coon = await con();
});

afterAll(async () => {
  coon.close();
});

test("Record New User", async (done) => {
  const data = {
    username: faker.internet.userName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
  };
  supertest(app.app)
    .post("/api/user/")
    .send(data)
    .set("Accept", "application/json")
    .expect(200)
    .then((res) => {
      expect(res.status).toEqual(200);
      expect(res.body).toEqual({
        data: {
          message: "User has been created",
          status: "Success",
          statusCode: 201,
        },
      });
      return done();
    })
    .catch((err) => {
      console.log(err);
    });
});

test("Login User", async (done) => {
  const filters = await UserEntity.getRepository().findOne();
  supertest(app.app)
    .post("/api/user/login/")
    .send({
      username: filters.username,
      password: "Password",
    })
    .set("Accept", "application/json")
    .expect(200)
    .then((res) => {
      expect(res.status).toEqual(200);
      expect(res.body).not.toEqual(null);
      return done();
    });
});

test("Forgotted User", async () => {
  const filters = await UserEntity.getRepository().findOne({
    order: {
      id: "ASC",
    },
  });
  supertest(app.app)
    .post("/api/user/forgotted/")
    .send({
      token: filters.username,
    })
    .expect(200)
    .then((res) => {
      expect(res.status).toEqual(200);
      expect(res.body).toEqual({
        data: {
          message:
            "Check your email for a link to reset your password. If it doesn’t appear within a few minutes, check your spam folder.",
          status: "Success",
          statusCode: 200,
        },
      });
    });
});

test("GET All User", async (done) => {
  supertest(app.app)
    .get("/api/user/")
    .set("Accept", "application/json")
    .expect(200)
    .then((res) => {
      expect(res.body).not.toEqual(null);
      expect(res.status).toEqual(200);
      return done();
    });
});

test("Delete User", async (done) => {
  const filters = await UserEntity.getRepository().findOne({
    order: {
      id: "DESC",
    },
  });
  const count = await UserEntity.count();
  if (count >= 10) {
    supertest(app.app)
      .delete("/api/user/" + filters.id + "/")
      .expect(200)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body).toEqual({
          data: {
            message: "Accounts has been deleted",
            statusCode: 200,
            status: "Success",
          },
        });
        return done();
      });
  }
});

test("Update User", async (done) => {
  const data = await UserEntity.getRepository().findOne({
    order: {
      id: "ASC",
    },
  });
  if (data) {
    supertest(app.app)
      .post("/api/user/update/")
      .send({
        id: data.id,
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
      })
      .expect(200)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body).toEqual({
          data: {
            message: "Profile has been updated",
            status: "Success",
            statusCode: 200,
          },
        });
        return done();
      });
  }
});
