import "reflect-metadata";
import express, { NextFunction, Request, Response } from "express";
import webpack from "webpack";
import middleware from "webpack-dev-middleware";
import hotMiddleware from "webpack-hot-middleware";
import config from "../../conf/webpack.common";
import devConfig from "../../conf/webpack.dev";
import dotenv from "dotenv";
import jwt from "express-jwt";
import fs from "fs";
import { useContainer } from "typeorm";
import { UserController } from "../controller/userController";
import cors from "cors";
import { ProductController } from "../controller/productController";
import multer from "multer";
import { Container } from "typeorm-typedi-extensions";
import { transpoter } from "./transpoterConfig";
import { CategoryController } from "../controller/CategoryController";
import path from "path";
import morgan from "morgan";
const bodyParser = require("body-parser");

dotenv.config();

export class Application {
  public app: express.Application;
  public port: number = parseInt(process.env.port) || 8000;
  public __prod__: string = process.env.prod || "false";
  private privateKey: string = fs.readFileSync("jwtRS256.key", "utf-8");
  public update = multer();

  constructor() {
    useContainer(Container);

    this.app = express();
    this.middleware();
  }

  private middleware() {
    if (this.__prod__ === "false") {
      if (!process.env.TESTING) {
        this.webpackMiddlewre();
        transpoter.verify(function (err, res) {
          if (err) {
            console.log("Cannot to connect to SMTP");
          } else {
            console.log("Connection SMTP");
          }
        });
      }
    }

    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(
      "/uploads",
      express.static(path.join(__dirname, "../../uploads"))
    );
    this.app.use(
      morgan("common", {
        stream: fs.createWriteStream(path.join(__dirname, "../../access.log"), {
          flags: "a",
        }),
      })
    );
    this.app.use(cors());

    this.app.use(
      jwt({
        secret: this.privateKey,
        algorithms: ["RS256"],
        credentialsRequired: false,
        getToken: function fromHeaderOrQuerystring(req) {
          if (
            req.headers.authorization &&
            req.headers.authorization.split(" ")[0] === "Bearer"
          ) {
            return req.headers.authorization.split(" ")[1];
          } else if (req.query && req.query.token) {
            return req.query.token;
          }
          return null;
        },
      })
    );
    this.routerMiddleware();
  }

  private webpackMiddlewre() {
    const compiler = webpack(devConfig);
    this.app.use(
      middleware(compiler, {
        publicPath: config.output.publicPath,
      })
    );
    this.app.use(hotMiddleware(compiler));
  }

  public authenticateToken(req: Request, res: Response, next: NextFunction) {
    if (!req.user) {
      return res.sendStatus(401);
    }
    next();
  }

  async routerMiddleware() {
    const multerStorage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, "uploads");
      },
      filename: (req, file, cb) => {
        const ext = file.mimetype.split("/")[1];
        cb(null, `${file.fieldname}-${Date.now()}.${ext}`);
      },
    });

    const upload = multer({ storage: multerStorage });
    const userCtrl = new UserController();
    const productCtrl = new ProductController(
      {
        authenticate: this.authenticateToken,
      },
      upload
    );
    const categoryCtrl = new CategoryController({
      authenticate: this.authenticateToken,
    });
    this.app.use(userCtrl.initialUserRouter());
    this.app.use(productCtrl.initialProductRouter());
    this.app.use(categoryCtrl.initialCategoryRouter());
  }

  public listen() {
    if (!process.env.TESTING) {
      this.app.listen(this.port, () => {
        console.log("application running");
      });
    }
  }
}
