import nodemailer, { Transporter } from "nodemailer";
import dotenv from "dotenv";

dotenv.config();

export let transpoter: Transporter = nodemailer.createTransport({
  pool: true,
  host: process.env.smtp_host,
  port: parseInt(process.env.smtp_port),
  secure: true,
  auth: {
    user: process.env.smtp_user,
    pass: process.env.smtp_pass,
  },
});

export const connectTranspoter = (args: any) => {
  transpoter.sendMail({
    from: process.env.smtp_user,
    to: args,
    subject: "Reset Password",
    text: "Hello Worlds",
  });
  return transpoter;
};
