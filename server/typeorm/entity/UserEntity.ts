import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import bcrypt from "bcrypt";
import { ProductEntity } from "./ProductEntity";

@Entity("user")
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  readonly id: string;

  @Column("varchar", { nullable: false, unique: true, length: 225 })
  username: string;
  @Column("varchar", { nullable: false, unique: true, length: 225 })
  email: string;
  @Column("varchar", { nullable: true, length: 225 })
  avatar: string;
  @Column("varchar", { nullable: true, length: 225 })
  first_name: string;
  @Column("varchar", { nullable: true, length: 225 })
  last_name: string;
  @Column("varchar", { nullable: false, length: 225 })
  password: string;
  @Column("timestamp", { nullable: false })
  createAt: Date;
  @Column("timestamp", { nullable: false, onUpdate: "CURRENT_TIMESTAMP" })
  updateAt: Date;
  @OneToMany(() => ProductEntity, (product) => product.accounts, {
    cascade: true,
  })
  @JoinColumn()
  product: ProductEntity[];

  @BeforeInsert()
  async generateCreateAt() {
    this.createAt = new Date();
  }
  @BeforeInsert()
  async generateUpdateAt() {
    this.updateAt = new Date();
  }

  @BeforeInsert()
  async generatePassword() {
    this.password = await bcrypt.hash(
      this.password,
      Math.floor((Math.random() + 1) * Math.random())
    );
  }

  @BeforeUpdate()
  async retrieveUpdateAt() {
    this.updateAt = new Date();
  }

  async dataRecord(args) {
    await this.save(args);
  }

  async checkPassword(args) {
    const check = await bcrypt.compareSync(args, this.password);
    return check;
  }
}
