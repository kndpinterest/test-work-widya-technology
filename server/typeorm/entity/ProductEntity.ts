import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { CategoryProductEntity } from "./CategoryProductEntity";
import { UserEntity } from "./UserEntity";

@Entity("product")
export class ProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  readonly id: string;
  @Column("varchar", { nullable: false, unique: true, length: 225 })
  product_name: string;
  @Column("varchar", { nullable: false, length: 225 })
  type: string;
  @Column("varchar", { nullable: false, length: 225 })
  item: string;
  @Column("varchar", { nullable: false, length: 225 })
  weight: string;
  @Column("varchar", { nullable: false, length: 225 })
  sku: string;
  @Column("decimal", { precision: 12, scale: 2 })
  price_sell: string;
  @Column("decimal", { precision: 12, scale: 2 })
  price_promo: string;
  @Column("decimal", { precision: 12, scale: 2 })
  price_agent: string;
  @Column("varchar", { nullable: false, length: 300 })
  photo: string;
  @Column("varchar", { nullable: false, length: 225 })
  recommendation: string;
  @Column("text", { nullable: true })
  description: string;
  @Column("int", { nullable: false, default: 1 })
  status: string;
  @Column("varchar", { nullable: true, length: 225 })
  ordering: string;

  @ManyToOne(() => UserEntity, (user) => user.product)
  @JoinColumn()
  accounts: UserEntity;

  @ManyToOne(
    (type) => CategoryProductEntity,
    (category) => category.product_category
  )
  @JoinColumn()
  category: CategoryProductEntity;

  @Column("timestamp", { nullable: false })
  createAt: Date;
  @Column("timestamp", { nullable: false, onUpdate: "CURRENT_TIMESTAMP" })
  updateAt: Date;

  @BeforeInsert()
  async generateCreateAt() {
    this.createAt = new Date();
  }

  @BeforeInsert()
  async generateUpdateAt() {
    this.updateAt = new Date();
  }
}
