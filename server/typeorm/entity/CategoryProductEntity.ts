import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { ProductEntity } from "./ProductEntity";

@Entity("category_product")
export class CategoryProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  readonly id: string;
  @Column("varchar", { nullable: false, length: 225 })
  name: string;
  @Column("varchar", { nullable: true, length: 225 })
  icon: string;
  @Column("varchar", { nullable: true, length: 225 })
  icon_web: string;
  @Column("varchar", { nullable: true, length: 225 })
  status: string;
  @Column("varchar", { nullable: true, length: 225 })
  ordering: string;
  @Column("timestamp", { nullable: false })
  createAt: Date;
  @Column("timestamp", { nullable: false, onUpdate: "CURRENT_TIMESTAMP" })
  updateAt: Date;

  @OneToMany((type) => ProductEntity, (product) => product.category, {
    nullable: true,
  })
  @JoinColumn()
  product_category: ProductEntity[];

  @BeforeInsert()
  async generatecreateAt() {
    this.createAt = new Date();
  }

  @BeforeInsert()
  async generateIpdateAt() {
    this.updateAt = new Date();
  }
}
