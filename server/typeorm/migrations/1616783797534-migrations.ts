import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1616783797534 implements MigrationInterface {
    name = 'migrations1616783797534'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` DROP FOREIGN KEY `FK_e5b8302a3d13ef7c64dc92e90f9`");
        await queryRunner.query("ALTER TABLE `product` CHANGE `accountsId` `accountsId` varchar(36) NOT NULL");
        await queryRunner.query("ALTER TABLE `product` ADD CONSTRAINT `FK_e5b8302a3d13ef7c64dc92e90f9` FOREIGN KEY (`accountsId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` DROP FOREIGN KEY `FK_e5b8302a3d13ef7c64dc92e90f9`");
        await queryRunner.query("ALTER TABLE `product` CHANGE `accountsId` `accountsId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `product` ADD CONSTRAINT `FK_e5b8302a3d13ef7c64dc92e90f9` FOREIGN KEY (`accountsId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

}
