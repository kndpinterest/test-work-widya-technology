import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1616789047592 implements MigrationInterface {
    name = 'migrations1616789047592'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` ADD CONSTRAINT `FK_e5b8302a3d13ef7c64dc92e90f9` FOREIGN KEY (`accountsId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `product` ADD CONSTRAINT `FK_ff0c0301a95e517153df97f6812` FOREIGN KEY (`categoryId`) REFERENCES `category_product`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` DROP FOREIGN KEY `FK_ff0c0301a95e517153df97f6812`");
        await queryRunner.query("ALTER TABLE `product` DROP FOREIGN KEY `FK_e5b8302a3d13ef7c64dc92e90f9`");
    }

}
