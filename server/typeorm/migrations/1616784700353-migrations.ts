import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1616784700353 implements MigrationInterface {
    name = 'migrations1616784700353'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `category_product` CHANGE `name` `name` varchar(225) NOT NULL");
        await queryRunner.query("ALTER TABLE `category_product` CHANGE `createAt` `createAt` timestamp NOT NULL");
        await queryRunner.query("ALTER TABLE `category_product` CHANGE `updateAt` `updateAt` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `category_product` CHANGE `updateAt` `updateAt` timestamp NULL ON UPDATE CURRENT_TIMESTAMP");
        await queryRunner.query("ALTER TABLE `category_product` CHANGE `createAt` `createAt` timestamp NULL");
        await queryRunner.query("ALTER TABLE `category_product` CHANGE `name` `name` varchar(225) NULL");
    }

}
