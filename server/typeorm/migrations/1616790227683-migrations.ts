import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1616790227683 implements MigrationInterface {
    name = 'migrations1616790227683'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP INDEX `REL_e5b8302a3d13ef7c64dc92e90f` ON `product`");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE UNIQUE INDEX `REL_e5b8302a3d13ef7c64dc92e90f` ON `product` (`accountsId`)");
    }

}
