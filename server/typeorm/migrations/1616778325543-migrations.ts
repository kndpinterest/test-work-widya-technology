import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1616778325543 implements MigrationInterface {
    name = 'migrations1616778325543'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` DROP COLUMN `photo`");
        await queryRunner.query("ALTER TABLE `product` ADD `photo` varchar(255) NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` DROP COLUMN `photo`");
        await queryRunner.query("ALTER TABLE `product` ADD `photo` varchar(225) NOT NULL");
    }

}
