import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1616600536430 implements MigrationInterface {
    name = 'migrations1616600536430'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `username` varchar(225) NOT NULL, `email` varchar(225) NOT NULL, `avatar` varchar(225) NULL, `first_name` varchar(225) NULL, `last_name` varchar(225) NULL, `password` varchar(225) NOT NULL, `createAt` timestamp NOT NULL, `updateAt` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP, UNIQUE INDEX `IDX_78a916df40e02a9deb1c4b75ed` (`username`), UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `product` (`id` varchar(36) NOT NULL, `product_name` varchar(225) NOT NULL, `type` varchar(225) NOT NULL, `item` varchar(225) NOT NULL, `weight` varchar(225) NOT NULL, `sku` varchar(225) NOT NULL, `price_sell` varchar(225) NOT NULL, `price_promo` varchar(225) NOT NULL, `price_agent` varchar(225) NOT NULL, `photo` varchar(225) NOT NULL, `recommendation` varchar(225) NOT NULL, `description` varchar(225) NOT NULL, `status` varchar(225) NOT NULL, `ordering` varchar(225) NOT NULL, `createAt` timestamp NOT NULL, `updateAt` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP, `accountsId` varchar(36) NULL, UNIQUE INDEX `IDX_aff16b2dbdb8fa56d29ed91e28` (`product_name`), UNIQUE INDEX `REL_e5b8302a3d13ef7c64dc92e90f` (`accountsId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `category_product` (`id` varchar(36) NOT NULL, `name` varchar(225) NOT NULL, `icon` varchar(225) NULL, `icon_web` varchar(225) NULL, `status` varchar(225) NULL, `ordering` varchar(225) NULL, `createAt` timestamp NOT NULL, `updateAt` timestamp NOT NULL ON UPDATE CURRENT_TIMSTAMP, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `product` ADD CONSTRAINT `FK_e5b8302a3d13ef7c64dc92e90f9` FOREIGN KEY (`accountsId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `product` DROP FOREIGN KEY `FK_e5b8302a3d13ef7c64dc92e90f9`");
        await queryRunner.query("DROP TABLE `category_product`");
        await queryRunner.query("DROP INDEX `REL_e5b8302a3d13ef7c64dc92e90f` ON `product`");
        await queryRunner.query("DROP INDEX `IDX_aff16b2dbdb8fa56d29ed91e28` ON `product`");
        await queryRunner.query("DROP TABLE `product`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP INDEX `IDX_78a916df40e02a9deb1c4b75ed` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
    }

}
