import express, { Request, Response } from "express";
import { Router } from "express";
import { Multer } from "multer";
import { ProductInstance } from "../service/productService";
import { ProductResponse } from "../types/productTypes";
import { AuthToken } from "../types/userTypes";

export class ProductController {
  public path = "/api/product/";
  public router = Router();
  constructor(public auth: AuthToken, public upload: Multer) {
    this.initialProductRouter();
  }

  public initialProductRouter() {
    this.router.get(this.path, this.auth.authenticate, this.getAllProduct);
    this.router.post(
      this.path,
      this.upload.single("photo"),
      this.auth.authenticate,
      this.recordProduct
    );
    this.router.delete(
      `${this.path}:id/`,
      this.auth.authenticate,
      this.deleteProduct
    );
    this.router.post(
      `${this.path}:id/`,
      this.upload.single("photo"),
      this.auth.authenticate,
      this.updateProduct
    );
    this.router.get(
      `${this.path}:id/`,
      this.auth.authenticate,
      this.detailProduct
    );
    return this.router;
  }

  async recordProduct(
    req: express.Request,
    res: express.Response
  ): Promise<Response<ProductResponse>> {
    return await ProductInstance.recordProduct(req, res);
  }

  async deleteProduct(
    req: express.Request,
    res: express.Response
  ): Promise<Response<ProductResponse>> {
    return await ProductInstance.deleteProduct(req.params.id, res);
  }

  async updateProduct(
    req: express.Request,
    res: express.Response
  ): Promise<Response<ProductResponse>> {
    return await ProductInstance.updateProduct(req, res);
  }

  async getAllProduct(
    req: express.Request,
    res: express.Response
  ): Promise<Response<ProductResponse>> {
    return await ProductInstance.getAll(res);
  }

  async detailProduct(
    req: Request,
    res: Response
  ): Promise<Response<ProductResponse>> {
    return await ProductInstance.detailProduct(req.params.id, res);
  }
}
