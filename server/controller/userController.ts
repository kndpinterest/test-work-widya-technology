import express, { Response, Router } from "express";
import { Service } from "typedi";
import { UserInstance } from "../service/userService";
import { UserResponse } from "../types/userTypes";

@Service()
export class UserController {
  public path = "/api/user/";
  public router = Router();

  constructor() {
    this.initialUserRouter();
  }

  public initialUserRouter() {
    this.router.post(this.path, this.recordUser);
    this.router.get(this.path, this.getAll);
    this.router.post(`${this.path}login/`, this.loginUser);
    this.router.post(`${this.path}forgotted/`, this.forgottedUser);
    this.router.delete(`${this.path}:id/`, this.deleteUser);
    this.router.post(`${this.path}update/`, this.updateUser);
    return this.router;
  }

  async loginUser(
    request: express.Request,
    res: express.Response
  ): Promise<Response<UserResponse>> {
    return await UserInstance.loginUser(request.body, res);
  }

  async getAll(
    request: express.Request,
    res: express.Response
  ): Promise<Response<UserResponse>> {
    return await UserInstance.allUser(res);
  }

  async recordUser(
    request: express.Request,
    res: express.Response
  ): Promise<Response<UserResponse>> {
    const results = await UserInstance.createUser(request.body, res);
    return results;
  }

  async forgottedUser(
    request: express.Request,
    res: express.Response
  ): Promise<Response<UserResponse>> {
    return await UserInstance.resetUser(request.body, res);
  }

  async deleteUser(
    request: express.Request,
    res: express.Response
  ): Promise<Response<UserResponse>> {
    return await UserInstance.destroyUser(request.params.id, res);
  }

  async updateUser(
    request: express.Request,
    res: express.Response
  ): Promise<Response<UserResponse>> {
    return await UserInstance.updateUser(request.body, res);
  }
}
