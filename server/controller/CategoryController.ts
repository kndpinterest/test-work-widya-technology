import { Request, Response, Router } from "express";
import { CategoryInstance } from "../service/categoryService";
import { CategoryResponse } from "../types/categoryTypes";
import { AuthToken } from "../types/userTypes";

export class CategoryController {
  public path = "/api/category/";
  constructor(public auth: AuthToken) {
    this.initialCategoryRouter();
  }

  public initialCategoryRouter() {
    const router = Router();
    router.get(this.path, this.auth.authenticate, this.getAll);
    router.post(this.path, this.auth.authenticate, this.recordCategory);
    router.delete(
      `${this.path}:id/`,
      this.auth.authenticate,
      this.deleteCategory
    );
    router.put(`${this.path}:id/`, this.auth.authenticate, this.updateCategory);
    router.get(`${this.path}:id/`, this.auth.authenticate, this.detailCategory);
    return router;
  }

  async getAll(
    req: Request,
    res: Response
  ): Promise<Response<CategoryResponse>> {
    return await CategoryInstance.getAll(res);
  }

  async recordCategory(
    req: Request,
    res: Response
  ): Promise<Response<CategoryResponse>> {
    return await CategoryInstance.recordCategory(req.body, res);
  }

  async deleteCategory(
    req: Request,
    res: Response
  ): Promise<Response<CategoryResponse>> {
    return await CategoryInstance.destroyCategory(req.params.id, res);
  }

  async detailCategory(
    req: Request,
    res: Response
  ): Promise<Response<CategoryResponse>> {
    return await CategoryInstance.detailCategory(req.params.id, res);
  }

  async updateCategory(
    req: Request,
    res: Response
  ): Promise<Response<CategoryResponse>> {
    return await CategoryInstance.updateCategory(req.params.id, req.body, res);
  }
}
