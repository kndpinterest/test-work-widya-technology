import { ProductEntity } from "../typeorm/entity/ProductEntity";
import { Status } from "./userTypes";

export interface ProductResponse {
  status: Status;
  statusCode: number;
  result?: ProductEntity[];
  data?: ProductEntity;
  message?: string;
}
