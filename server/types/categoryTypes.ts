import { CategoryProductEntity } from "../typeorm/entity/CategoryProductEntity";
import { Status } from "./userTypes";

export interface CategoryResponse {
  status: Status;
  statusCode: number;
  result?: CategoryProductEntity[];
  data?: CategoryProductEntity;
  message?: string;
}
