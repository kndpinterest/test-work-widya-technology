import { NextFunction, Request, Response } from "express";
import { UserEntity } from "../typeorm/entity/UserEntity";

export type Status = "Success" | "Error";

export interface RequestUser {
  user: UserEntity
}

export interface UserResponse {
  result?: UserEntity[];
  data: UserEntity;
  statusCode: number;
  status: Status;
  message?: string;
  token?: string;
}

export interface AuthToken {
  authenticate(req: Request, res: Response, next: NextFunction): void;
}
