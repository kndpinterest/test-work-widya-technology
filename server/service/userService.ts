import { Response } from "express";
import Container, { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";
import { UserEntity } from "../typeorm/entity/UserEntity";
import { Status } from "../types/userTypes";
import jwt from "jsonwebtoken";
import fs from "fs";
import { connectTranspoter } from "../utils/transpoterConfig";

@Service()
@EntityRepository(UserEntity)
class UserService extends Repository<UserEntity> {
  async loginUser(args: any, res: Response) {
    const privateKey = fs.readFileSync("jwtRS256.key", "utf-8");
    let status: Status = "Error",
      statusCode: number = 404,
      message: string = "Inccorect Username or password",
      token: string;
    const check = await UserEntity.findOne({
      where: {
        username: args.username,
      },
    });
    if (check) {
      if (check.checkPassword(args.password)) {
        token = await jwt.sign({ user: check }, privateKey, {
          algorithm: "RS256",
        });
        message = "";
        status = "Success";
        statusCode = 200;
      }
      if (process.env.TESTING) {
        fs.writeFileSync("token.txt", token);
      }
    }
    return await res.json({
      status,
      statusCode,
      token,
      message,
    });
  }

  async createUser(options: any, res: Response) {
    let statusCode: number = 400,
      status: Status = "Error",
      message = "Username or Email already, exists please choose another one.";
    // Filter User
    const filters = await UserEntity.findOne({
      where: [
        {
          username: options.username,
        },
        {
          email: options.email,
        },
      ],
    });
    if (!filters) {
      const createUser = new UserEntity();
      createUser.username = options.username;
      createUser.email = options.email;
      createUser.password = options.password;
      await createUser.dataRecord(createUser);
      message = "User has been created";
      statusCode = 201;
      status = "Success";
    }

    return res.json({
      data: {
        status,
        statusCode,
        message,
      },
    });
  }

  async allUser(res: Response) {
    let status: Status = "Success",
      statusCode: number = 200;
    return res.json({
      data: {
        status,
        statusCode,
        result: await UserEntity.createQueryBuilder().getMany(),
      },
    });
  }

  async destroyUser(args: any, res: Response) {
    let status: Status = "Error",
      statusCode: number = 404,
      message = "Accounts not found";
    const filters = await UserEntity.findOne({ where: { id: args } });

    if (filters) {
      await UserEntity.getRepository().delete(filters);
      status = "Success";
      statusCode = 200;
      message = "Accounts has been deleted";
    }
    return res.json({
      data: {
        status,
        statusCode,
        message,
      },
    });
  }

  async resetUser(args: any, res: Response) {
    let status: Status = "Error",
      statusCode: number = 404,
      message: string = "Accounts not found";
    const check = await UserEntity.findOne({
      where: [
        {
          username: args.token,
        },
        {
          email: args.token,
        },
      ],
    });
    if (check) {
      connectTranspoter(check.email);
      message =
        "Check your email for a link to reset your password. If it doesn’t appear within a few minutes, check your spam folder.";
      statusCode = 200;
      status = "Success";
    }
    return res.json({
      data: {
        message,
        status,
        statusCode,
      },
    });
  }

  async updateUser(args: any, res: Response) {
    let message: string = "Accounts not found",
      status: Status = "Error",
      statusCode: number = 404;

    const check = await UserEntity.findOne({
      where: {
        id: args.id,
      },
    });

    if (check) {
      check.first_name = args.first_name;
      check.last_name = args.last_name;
      await UserEntity.getRepository().update(check.id, check);
      message = "Profile has been updated";
      status = "Success";
      statusCode = 200;
    }

    return res.json({
      data: {
        message,
        status,
        statusCode,
      },
    });
  }
}

export const UserInstance = Container.get(UserService);
