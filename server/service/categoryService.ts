import { Response } from "express";
import Container, { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";
import { CategoryProductEntity } from "../typeorm/entity/CategoryProductEntity";
import { Status } from "../types/userTypes";

@Service()
@EntityRepository(CategoryProductEntity)
export class CategoryService extends Repository<CategoryProductEntity> {
  async getAll(res: Response) {
    let status: Status = "Success",
      statusCode: number = 200;
    return res.json({
      status,
      statusCode,
      result: await CategoryProductEntity.getRepository()
        .createQueryBuilder("category")
        .leftJoinAndSelect("category.product_category", "product")
        .getMany(),
    });
  }

  async recordCategory(args: any, res: Response) {
    let status: Status = "Success",
      statusCode: number = 201,
      message: string = "Category Product has been created";
    const product = new CategoryProductEntity();
    product.name = args.name;
    await CategoryProductEntity.getRepository().save(product);
    return res.json({
      data: {
        status,
        statusCode,
        message,
      },
    });
  }

  async destroyCategory(params: any, res: Response) {
    let status: Status = "Error",
      statusCode: number = 400,
      message: string = "Category Not Found";
    const check = await CategoryProductEntity.findOne({
      where: { id: params },
    });
    if (check) {
      status = "Success";
      statusCode = 200;
      message = "Category Product has been deleted";
      await CategoryProductEntity.getRepository().delete(check);
    }
    return res.json({
      data: {
        message,
        status,
        statusCode,
      },
    });
  }

  async updateCategory(params: string, args: any, res: Response) {
    let status: Status = "Error",
      statusCode: number = 400,
      message: string = "Category Not Found";
    const check = await CategoryProductEntity.findOne({
      where: { id: params },
    });
    if (check) {
      message = "Category Product has been updated";
      status = "Success";
      statusCode = 200;
      check.name = args.name;
      await CategoryProductEntity.getRepository().update(check.id, check);
    }
    return res.json({
      data: {
        message,
        status,
        statusCode,
      },
    });
  }

  async detailCategory(params: any, res: Response) {
    let status: Status = "Error",
      statusCode: number = 400,
      message: string = "Category Not Found";
    const check = await CategoryProductEntity.findOne(params);
    if (check) {
      message = "";
      status = "Success";
      statusCode = 200;
    }
    return res.json({
      data: {
        message,
        status,
        statusCode,
        data: check,
      },
    });
  }
}

export const CategoryInstance = Container.get(CategoryService);
