import { Request, Response } from "express";
import Container, { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";
import { CategoryProductEntity } from "../typeorm/entity/CategoryProductEntity";
import { ProductEntity } from "../typeorm/entity/ProductEntity";
import { RequestUser, Status } from "../types/userTypes";

@Service()
@EntityRepository(ProductEntity)
export class ServiceProduct extends Repository<ProductEntity> {
  async getAll(res: Response) {
    let status: Status = "Success",
      statusCode: number = 200;
    return res.json({
      data: {
        status,
        statusCode,
        result: await ProductEntity.getRepository()
          .createQueryBuilder("product")
          .leftJoinAndSelect("product.category", "category")
          .getMany(),
      },
    });
  }

  async recordProduct(args: Request, res: Response) {
    let status: Status, statusCode: number, message: string;
    const cc = await CategoryProductEntity.getRepository().findOne({
      where: {
        name: args.body.category,
      },
    });
    let sku = "0000000";
    const replace = sku.replace(`${args.body.sku.length}`, "") + args.body.sku;
    if (!cc) {
      message = "Category Product not found";
      statusCode = 404;
      status = "Error";
    } else {
      const product = new ProductEntity();
      product.product_name = args.body.product_name;
      product.category = cc;
      product.type = args.body.type;
      product.item = args.body.item;
      product.price_sell = args.body.price_sell;
      product.price_promo = args.body.price_promo;
      product.price_agent = args.body.price_agent;
      product.accounts = (args.user as RequestUser).user;
      product.weight = args.body.weight;
      product.sku = replace;
      product.photo = args.file.filename;
      product.recommendation = args.body.recommendation;
      product.description = args.body.description;
      await ProductEntity.getRepository().save(product);
      message = "Product has been created";
      statusCode = 201;
      status = "Success";
    }

    return res.json({
      data: {
        message,
        status,
        statusCode,
      },
    });
  }

  async detailProduct(params: string, res: Response) {
    let statusCode: number = 404,
      status: Status = "Error",
      message: string = "Product not found";
    const check = await ProductEntity.findOne({ where: { id: params } });
    if (check) {
      status = "Success";
      statusCode = 200;
      message = "";
    }
    return res.json({
      data: {
        status,
        statusCode,
        message,
        data: check,
      },
    });
  }

  async updateProduct(req: Request, res: Response) {
    let statusCode: number = 404,
      status: Status = "Error",
      message: string = "Product not found";
    const check = await ProductEntity.findOne({ where: { id: req.params.id } });
    if (check) {
      message = "Product has been updated";
      statusCode = 200;
      status = "Success";
      check.product_name = req.body.product_name;
      check.type = req.body.type;
      check.item = req.body.item;
      check.price_sell = req.body.price_sell;
      check.price_promo = req.body.price_promo;
      check.price_agent = req.body.price_agent;
      check.weight = req.body.weight;
      check.photo = req.file.filename;
      await ProductEntity.getRepository().update(check.id, check);
    }
    return res.json({
      data: {
        status,
        statusCode,
        message,
      },
    });
  }

  async deleteProduct(params: string, res: Response) {
    let statusCode: number = 404,
      status: Status = "Error",
      message: string = "Product not found";
    const check = await ProductEntity.findOne({ where: { id: params } });
    if (check) {
      message = "Product has been deleted";
      statusCode = 200;
      status = "Success";
      await ProductEntity.getRepository().delete(check);
    }
    return res.json({
      data: {
        status,
        statusCode,
        message,
      },
    });
  }
}

export const ProductInstance = Container.get(ServiceProduct);
